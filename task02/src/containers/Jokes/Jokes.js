import React, {Component, Fragment} from 'react';
import './Jokes.css';
import Button from "../../components/ChucknorrisJokes/Button/Button";
import ChucknorrisJokes from "../../components/ChucknorrisJokes/ChucknorrisJokes";

class Jokes extends Component {
	
	state = {
		jokes: []
	};
	
	getNewJokes = async() => {
		const jokes = [];
		while (jokes.length < 5) {
			const response = await fetch('https://api.chucknorris.io/jokes/random');
			if (response.ok) {
				let updatedJokes = await response.json();
				jokes.push(updatedJokes);
			} else {
				console.log('Network error');
			}
		}
		Promise.all(jokes).then(() => {
			console.log(jokes);
			this.setState({jokes});
		});
	};
	
	componentDidMount() {this.getNewJokes()}
	
	render() {
		return (
			<Fragment>
				<ChucknorrisJokes jokes={this.state.jokes}/>
				<Button newJokes={this.getNewJokes}/>
			</Fragment>
		)
	}
}

export default Jokes;