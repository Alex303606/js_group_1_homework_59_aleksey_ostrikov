import React from 'react'

const Joke = props => {
	return (
		<div className="Joke">
			<img src={props.image} alt=""/>
			<p>{props.text}</p>
		</div>
	)
};

export default Joke;