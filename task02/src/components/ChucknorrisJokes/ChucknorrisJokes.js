import React from 'react'
import Joke from "./Joke/Joke";

const ChucknorrisJokes = props => {
	return (
		<div className="ChucknorrisJokes">
			{
				props.jokes.map(joke => {
					return <Joke
						key={joke.id}
						image={joke.icon_url}
						text={joke.value}
					/>
				})
			}
		</div>
	)
};

export default ChucknorrisJokes;