import React, {Component} from 'react'
import './MyFilmsList.css';
import FilmsList from "../../components/FilmsList/FilmsList";
import Header from "../../components/FilmsList/Header/Header";


class MyFilmsList extends Component {
	
	state = {
		films: [],
		filmName: 'Film name',
	};
	
	changeName = (event) => {
		let filmName = this.state.filmName;
		filmName = event.target.value;
		this.setState({filmName});
	};
	
	addFilm = () => {
		const films = [...this.state.films];
		let filmName = this.state.filmName;
		let random = filmName + Math.random();
		let newfilm = {name: filmName, id: random};
		films.push(newfilm);
		this.setState({films});
	};
	
	deleteFilm = (id) => {
		const films = [...this.state.films];
		const index = films.findIndex(p => p.id === id);
		films.splice(index, 1);
		this.setState({films});
	};
	
	resetName = () => {
		let filmName = this.state.filmName;
		filmName = '';
		this.setState({filmName});
	};
	
	editFilmName = (event, id) => {
		const films = [...this.state.films];
		const index = films.findIndex(p => p.id === id);
		const film = {...this.state.films[index]};
		film.name = event.target.value;
		films[index] = film;
		this.setState({films});
	};
	
	componentDidMount() {
		this.setState({films: JSON.parse(localStorage.getItem('MyFilmsList')) || []});
	}
	
	componentDidUpdate() {
		localStorage.setItem('MyFilmsList', JSON.stringify(this.state.films));
	}
	
	render() {
		return (
			<div className="MyFilmsList">
				<Header
					changeName={(event) => this.changeName(event)}
					addFilm={() => this.addFilm()}
					filmName={this.state.filmName}
					resetName={(event) => this.resetName(event)}
				/>
				<FilmsList
					films={this.state.films}
					deleteFilm={this.deleteFilm}
					editName={this.editFilmName}
				/>
			</div>
		)
	}
}

export default MyFilmsList;