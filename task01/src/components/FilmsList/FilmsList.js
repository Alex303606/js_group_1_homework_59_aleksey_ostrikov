import React from 'react'
import Films from "./Films/Films";

const FilmsList = props => {
	return (
		<div className="content">
			<Films editName={props.editName} films={props.films} deleteFilm={props.deleteFilm}/>
		</div>
	)
};

export default FilmsList;