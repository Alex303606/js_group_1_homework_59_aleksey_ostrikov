import React from 'react';
import Film from "./Film/Film";

const Films = props => {
	return (
		<ul>
			{
				props.films.map(film => {
					return <Film
						key={film.id}
						name={film.name}
						deleteFilm={props.deleteFilm}
						id={film.id}
						editName={(event) => props.editName(event, film.id)}
					/>;
				})
			}
		</ul>
	)
};

export default Films;