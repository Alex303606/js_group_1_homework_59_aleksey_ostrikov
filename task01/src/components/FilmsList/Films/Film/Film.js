import React, {Component} from 'react';

class Film extends Component {
	
	shouldComponentUpdate(newProps, newState) {
		return (newProps.name !== this.props.name);
	}
	
	render() {
		return (
			<li>
				<input value={this.props.name} onChange={this.props.editName} className="name"/>
				<button
					onClick={() => this.props.deleteFilm(this.props.id)}
					className="delete">
					<i className="fas fa-times" aria-hidden="true"></i>
				</button>
			</li>
		)
	}
}

export default Film;